<?php

/*
Boucler sur les 3 comparations à faire
Exploder chaque fichier
Recupérer & stocker le string du produit à rechercher (index 1 de chaque input explodé)
Enlever l'index 0 et 1 du tableau explodé
Boucler sur tout ce qu'il reste dans le tableau :
    Pour chaque ligne, j'explode sur l'espace pour séparer le nom du produit du prix
    Si le nom du produit correspond à mon produit stocké :
        si le $minPrice actuel est de 0 : je remplace $minPrice par le prix du produit actuel
        sinon, si le prix actuel est inférieur au $minPrice, je remplace la valeur
J'affiche $minPrice
*/

$input[0] = explode(PHP_EOL, file_get_contents('input1.txt'));
$input[1] = explode(PHP_EOL, file_get_contents('input2.txt'));
$input[2] = explode(PHP_EOL, file_get_contents('input3.txt'));

/*EXO 9 NOV AC ANCIENS INPUT*/
/*$data = [];
foreach($input as $key=>$value){
    $product = $value[1];
    unset($value[0]);
    unset($value[1]);
    $minPrice = 0;
    foreach($value as $elem){
        $productData = explode(" ", $elem);
        if($productData[0] == $product){
            if($minPrice == 0){
                $minPrice = $productData[1];
            }
            else if($productData[1] < $minPrice){
                $minPrice = $productData[1];
            }
        }
    }
    echo '<strong>Prix minimal pour ' . $product . " est :</strong> " . $minPrice . "<br/><br/>";
}*/



/*UPDATE 16 NOV*/
// get only name of products of all files for display
$products = [];
for ($i = 0; $i < count($input); $i++) {
    foreach($input[$i] as $data){
        $productData = explode(" ", $data);
        if(!isset($products[$i][$productData[0]])){
            $products[$i][$productData[0]] = $productData[0];
        }
    }
}

// process all elements with command insert by user
foreach($products as $key=>$inputProducts){
    // Display which products r available for comparing
    echo 'Produits input '.($key+1). ' : \n\n';
    sort($inputProducts);
    print_r($inputProducts);

    // Ask user to enter id of element to compare
    $userInput = readline("Veuillez entrer l'identifiant du produit à comparer : ");
    $minPrice = 0;
    $productName = $inputProducts[$userInput];

    // search through all inputX.php data, returning lowest price
    foreach($input[$key] as $elem){
        $productData = explode(" ", $elem);
        if($productData[0] == $productName){
            if($minPrice == 0){
                $minPrice = $productData[1];
            }
            else if($productData[1] < $minPrice){
                $minPrice = $productData[1];
            }
        }
    }

    // search through all inputX.php data, returning all wanted products with DESC price
    $wantedProducts = [];
    foreach($input[$key] as $elem){
        $productData = explode(" ", $elem);
        if($productData[0] == $productName){
            $wantedProducts[] = ["name" => $productName, "price" => $productData[1]];
        }
    }
    // Order by price DESC all wanted products
    $price = array();
    foreach($wantedProducts as $k => $d) {
        $price[$k] = ["name" => $d['name'], "price" => $d['price']];
    }
    array_multisort($price, SORT_DESC, $wantedProducts);


    echo "Voici la liste des produits désirés, dans l'ordre décroissant de leurs prix : \n";
    print_r($price);

    echo "Le prix le plus bas pr le produit " . $productName . " est : " . $minPrice . " \n";
}

// récupération de tous les produits pr chaque entrée
    // foreach sur la donnée recup
        // Foreach sur chaque élement de donnée recup
            // Je récupère uniquement le nom du produit à l'index 0 de mon explode " "
                // Si valeur n'est pas ds mes porduits présents, je l'ajoute
// Afficher tableau