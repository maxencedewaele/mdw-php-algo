<?php

/* Calculer le nombre total de cases (multiplie : taille ferme x taille ferme)
Création variable $count = 0
On parse la matrice element par element:
    si element différent de "X" : $count = $count+ 1
Afficher le count total de cases cultivables
 */


$input[] = explode(PHP_EOL, file_get_contents('input1.txt'));
$input[] = explode(PHP_EOL, file_get_contents('input2.txt'));
$input[] = explode(PHP_EOL, file_get_contents('input3.txt'));
$input[] = explode(PHP_EOL, file_get_contents('input4.txt'));
$input[] = explode(PHP_EOL, file_get_contents('input5.txt'));

foreach($input as $key=>$farm){
    $taille = ($farm[0]*$farm[0]);
    $incubators = 0;
    $casesAvail = 0;
    unset($farm[0]);
    foreach($farm as $row){
        $characters = str_split($row);
        foreach($characters as $character){
            if($character == "X"){
                $incubators = $incubators + 1;
            }
        }
    }
    $casesAvail = ($taille - $incubators);
    echo "<strong>Ferme numéro " . ($key+1) . " dispose de : </strong>" . $casesAvail . " cases exploitables. <br/><br/>";
}

/*
for vertical
for horizontal

si . = si ds index a coté au dessus a cote en dessous incubateur : compte pour 1

*/

/*
foreach array
    foreach character
        si caractere : "." -> on recup son index dans le row
            utilise index pour recuperer colonne d'avant +  mm index    -> regarder si c'est un incibateur. si oui, stocker info + sortir boucle pr comptabiliser qu'une fois
            utilise index pour recuperer element à index - 1       -> regarder si c'est un incibateur. si oui, stocker info + sortir boucle pr comptabiliser qu'une fois
            utilise index pour recuperer element à index + 1      -> regarder si c'est un incibateur. si oui, stocker info + sortir boucle pr comptabiliser qu'une fois
            utilise index pr recuperer colonne d'après à l'index       -> regarder si c'est un incibateur. si oui, stocker info + sortir boucle pr comptabiliser qu'une fois



 */