<?php


/*$file = fopen('cartes_partie_1.txt', "r");*/
$input[] = explode(PHP_EOL, file_get_contents('cartes_partie_1.txt'));
$input[] = explode(PHP_EOL, file_get_contents('cartes_partie_2.txt'));
$input[] = explode(PHP_EOL, file_get_contents('cartes_partie_3.txt'));
foreach($input as $key=>$game){
    $rounds = $game[0];
    unset($game[0]);
    echo "<strong>Partie " . ($key + 1) . "</strong><br/><br/>";
    $countA = 0;
    $countB = 0;

    $bataille = false;
    $countBataille = 0;
    foreach($game as $row){
        $elems = explode(" ", $row);
        if ($elems[0] > $elems[1]) {
            $countA = ($countA +1);
            if($bataille === true){
                $countA = $countA + $countBataille;
                $countBataille = 0;
            }
            $bataille = false;
        } else if ($elems[0] < $elems[1]) {
            $countB = ($countB +1);
            if($bataille === true){
                $countB = $countB + $countBataille;
                $countBataille = 0;
            }
            $bataille = false;
        } else {
            $bataille = true;
            $countBataille = $countBataille + 1;
        }
    }

    /*echo "count A : " .$countA . "<br/>";
    echo "count B : " .$countB . "<br/>";*/

    if ($countA > $countB) {
        echo "Gagnant : A <br/><br/>";
    } else if ($countA < $countB) {
        echo "Gagnant : B <br/><br/>";
    } else {
        echo "Egalité entre les joueurs <br/><br/>";
    }
}

//nb carte de base = nb de tours (premiere ligne)
// SI meme carte
    // boolean bataille à true
    // Count bataille + 1
    // autre boucle : si bataille à true = regarder traitement normal
        // Gagnant = +1 + montant bataille